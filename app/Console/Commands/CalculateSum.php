<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CalculateSum extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the sum of an array which contains integers and other arrays with integers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayOfIntegers = [1,2,[3,4,[5]]];

        $this->info(array_sum(array_flatten($arrayOfIntegers)));
    }
}
