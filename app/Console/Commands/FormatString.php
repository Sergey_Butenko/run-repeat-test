<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

class FormatString extends Command
{
    const PLACEHOLDER_START = '{';
    const PLACEHOLDER_END = '}';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'format 
                            {originalString : string that needs to be formatted}
                            {valuesToReplacePlaceholders* : values to replace the placeholders} 
                            {--catchSkipped : Whether to catch skipped placeholders}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Format string and replace placeholders with given values.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws Exception
     */
    public function handle()
    {
        $originalString = $this->argument('originalString');
        $valuesToReplacePlaceholders = $this->argument('valuesToReplacePlaceholders');

        foreach ($valuesToReplacePlaceholders as $key => $value) {
            $originalString = str_ireplace(self::PLACEHOLDER_START . $key . self::PLACEHOLDER_END, $value, $originalString);
        }

        if($this->option('catchSkipped')) {
            $this->catchSkippedPlaceholders($originalString);
        }

        $this->info($originalString);
    }

    /**
     * Checks for any placeholders that are in the
     * string and then throws an Exception if one exists
     *
     * @param  string $string The string to check
     * @throws Exception
     */
    protected function catchSkippedPlaceholders(string $string): void
    {
        $matches = [];
        $pattern = "/" . self::PLACEHOLDER_START . ".*?" . self::PLACEHOLDER_END . "/";
        preg_match($pattern, $string, $matches);
        if(count($matches) > 0) {
            throw new Exception("Could not find a replacement for placeholder " . $matches[0], 1);
        }
    }
}
